from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy

from ecopex_app.forms import ContactForm
from ecopex_app.models import Event, Attractions, Projects, Partners, Service


def index(request, template_name='ecopex/index.html'):
    title = 'Ecopex Limited'
    projects = Projects.objects.all()
    partners = Partners.objects.all()
    service = Service.objects.all()
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Successfully submitted')
            return JsonResponse({
                'msg': 'Success'
            })
        else:
            messages.error(request, 'Error submitting your form')
            return JsonResponse({
                'msg': 'Error'
            })
    else:
        form = ContactForm()
    return render(request, template_name, locals())


def about(request, template_name='ecopex/about.html'):
    title = 'About'
    return render(request, template_name, locals())


def contact(request, template_name='ecopex/contact.html'):
    title = 'Contact'
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Successfully submitted')
            return HttpResponseRedirect(reverse_lazy('contact'))
        else:
            messages.error(request, 'Error submitting your form')
            return HttpResponseRedirect(reverse_lazy('contact'))
    else:
        form = ContactForm()

    return render(request, template_name, locals())


def services(request, template_name='ecopex/services.html'):
    title = 'Services'

    return render(request, template_name, locals())

def events(request, template_name='ecopex/events.html'):
    title = 'Events'
    latest_events = Event.objects.all().order_by('date')[:4]
    attractions = Attractions.objects.all()[:3]
    try:
        tour = Event.objects.filter(event_type=4).latest('date')
        attraction = Event.objects.filter(event_type=2).latest('date')
        evs = Event.objects.filter(event_type=3).latest('date')
        promotions = Event.objects.filter(event_type=1).latest('date')
    except Event.DoesNotExist:
        tour = None
        attraction = None
        evs = None
        promotions = None


    return render(request, template_name, locals())
