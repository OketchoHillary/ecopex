from django.apps import AppConfig


class EcopexAppConfig(AppConfig):
    name = 'ecopex_app'
