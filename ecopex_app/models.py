from django.db import models

class Event(models.Model):
    EVENT_CHOICE = (
        (1, 'Promotion'),
        (2, 'Attractions'),
        (3, 'Event'),
        (4, 'Tour')

    )
    name = models.CharField(max_length=250)
    detail = models.TextField(max_length=100)
    event_type = models.SmallIntegerField(default=0, choices=EVENT_CHOICE)
    date = models.DateField()
    time = models.TimeField()
    photo = models.ImageField(upload_to='media/event')

    def __str__(self):
        return self.name

    @property
    def photo_url(self):
        if self.photo and hasattr(self.photo, 'url'):
            return self.photo.url


class Contact_us(models.Model):
    names = models.CharField(max_length=120)
    email = models.EmailField()
    subject = models.CharField(max_length=250)
    message = models.TextField(max_length=500)

    def __str__(self):
        return self.subject

    class Meta:
        verbose_name_plural = 'Contacts'
        verbose_name = 'Contact'


class Attractions(models.Model):
    name = models.CharField(max_length=250)
    image = models.ImageField(upload_to='media/attractions', blank=True, null=True)
    description = models.TextField(max_length=500)

    def __str__(self):
        return self.name

    @property
    def photo_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url


    class Meta:
        verbose_name_plural = 'Attractions'
        verbose_name = 'Attraction'


class Service(models.Model):
    title = models.CharField(max_length=300)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Services'


class Projects(models.Model):
    title = models.CharField(max_length=300)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Projects'
        verbose_name = 'Project'


class Partners(models.Model):
    name = models.CharField(max_length=300)
    project = models.ForeignKey(Projects, blank=True, null=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Partners'
        verbose_name = 'Partner'


