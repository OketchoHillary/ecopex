from django.urls import path

from ecopex_app.views import index, about, contact, services, events

urlpatterns = [
    path('', index, name='index'),
    path('about/', about, name='about'),
    path('contact/', contact, name='contact'),
    path('services/', services, name='services'),
    path('events/', events, name='events'),

]