from django import  forms

from ecopex_app.models import Contact_us


class ContactForm(forms.ModelForm):

    class Meta:
        model = Contact_us
        fields = ['names', 'subject', 'email', 'message']