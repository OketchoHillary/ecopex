from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import Group

from ecopex_app.models import Projects, Contact_us, Partners, Service

admin.site.register(Contact_us)
admin.site.register(Projects)
admin.site.register(Partners)
admin.site.register(Service)

